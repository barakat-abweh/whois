
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.whois.WhoisClient;
import java.util.regex.Pattern;
import org.apache.commons.net.util.SubnetUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Barakat A. B. Abweh
 */
public class WhoIS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int argLen = args.length;
        if (argLen > 0) {
            if (argLen >= 1) {
                if (args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("--help")) {
                    printHelp();
                } else if (args[0].equalsIgnoreCase("-i") || args[0].equalsIgnoreCase("--ip")) {
                    if (argLen == 2) {
                        if (validIP(args[1])) {
                            whoIS(args[1]);
                        } else {
                            System.out.println("Please enter a valid IP Address eg: WhoIS -i 192.168.1.1");
                        }
                    } else {
                        System.out.println("Please enter a valid IP Address eg: WhoIS -i 192.168.1.1");
                    }
                } else if (args[0].equalsIgnoreCase("-s") || args[0].equalsIgnoreCase("--subnet")) {
                    if (argLen == 2) {
                        if (validSubnet(args[1])&&!validIP(args[1])) {
                            SubnetUtils utils = new SubnetUtils(args[1]);
                            String[] allIps = utils.getInfo().getAllAddresses();
                            for(String ip:allIps){
                                whoIS(ip);
                            }
                        }
                        else {
                        System.out.println("Please enter a valid subnet eg: WhoIS -s 192.168.1.0/24");
                    }
                    } else {
                        System.out.println("Please enter a valid subnet eg: WhoIS -s 192.168.1.0/24");
                    }
                } else if (args[0].equalsIgnoreCase("-d") || args[0].equalsIgnoreCase("--domain")) {
                    try {
                        InetAddress inetAddress = InetAddress.getByName(args[1]);
                        System.out.println(inetAddress.toString());
                        String ip = inetAddress.toString().split("/")[1];
                        if (validIP(ip)) {
                            whoIS(args[1]);
                        } else {
                            System.out.println("Please enter a valid domain eg: WhoIS -d google.com");
                        }
                    } catch (UnknownHostException ex) {
                        Logger.getLogger(WhoIS.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    printError();
                    printHelp();
                }
            }
        } else {
            printError();
            printHelp();
        }
    }
    
    private static void printError() {
        System.err.println("Please Enter a valid argument");
        printHelp();
    }
    
    private static void printHelp() {
        String help = "Tool designed to gather registration info for a specific Doman, IP or Subnet\n"
                + "WhoIS is designed to work in three modes\n"
                + "1. single IP\n"
                + "2. subnet\n"
                + "3. domain\n"
                + "usage :-\n"
                + "java -cp \".;.\\lib\\commons-net-3.8.0.jar\" WhoIS [arg] [ip/subnet/domain]\n"
                + "-i,--ip single ip address info eg:192.168.1.1\n"
                + "-s,--subnet whole subnet info eg:192.168.0/24\n"
                + "-d,--domain domain info eg:example.com\n"
                + "java WhoIS [arg] [ip/subnet/domain]\n"
                + "-i,--ip single ip address info eg:192.168.1.1\n"
                + "-s,--subnet whole subnet info eg:192.168.0/24\n"
                + "-d,--domain domain info eg:example.com";
        System.out.println(help);
    }
    
    private static boolean validIP(String ip) {
        String zeroTo255 = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])";
        String IP_REGEXP = zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255;
        Pattern IP_PATTERN = Pattern.compile(IP_REGEXP);
        return IP_PATTERN.matcher(ip).matches();
    }
    
    private static boolean validSubnet(String sub) {
        Pattern IP_PATTERN = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/([0-9]|[1-2][0-9]|3[0-2]))?$");
        return IP_PATTERN.matcher(sub).matches();
    }
    
    private static void whoIS(String ip) {
        StringBuilder result = new StringBuilder("");
        WhoisClient whois = new WhoisClient();
        try {
            whois.connect(WhoisClient.DEFAULT_HOST, WhoisClient.DEFAULT_PORT);
            String whoisData = whois.query("=" + ip);
            result.append(whoisData);
            whois.disconnect();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(result.toString());
    }
}
