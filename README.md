# WhoIS
Tool designed to gather registration info for a specific Doman, IP or Subnet
WhoIS is designed to work in three modes
1. single IP
2. subnet
3. domain
compilation :- javac -cp ".;.\lib\commons-net-3.8.0.jar" WhoIS.java
usage :-
java -cp ".;.\lib\commons-net-3.8.0.jar" WhoIS [arg] [ip/subnet/domain]
-i,--ip single ip address info eg:192.168.1.1
-s,--subnet whole subnet info eg:192.168.0/24
-d,--domain domain info eg:example.com
java WhoIS [arg] [ip/subnet/domain]
-i,--ip single ip address info eg:192.168.1.1
-s,--subnet whole subnet info eg:192.168.0/24
-d,--domain domain info eg:example.com
